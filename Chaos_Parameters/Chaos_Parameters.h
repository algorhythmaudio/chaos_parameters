/************************************************************************************************
 Global values for parameters
 
 Last Updated by: GW
 Last Updated on: 2012/11/17
 ************************************************************************************************/

#ifndef Chaos_Parameters_Chaos_Parameters_h
#define Chaos_Parameters_Chaos_Parameters_h

#include "AAFilterParams.h"

using namespace AADsp;


#pragma mark Global

enum {
    kParam1_GlobalChaosRate,
    kParam2_GlobalChaosScale,
    kParam3_FilterType,
    kParam4_FilterQ,
    kParam5_FilterChaosScale,
    kParam6_FilterChaosVar,
    kParam7_DistType,
    kParam8_DistChaosScale,
    kParam9_DistChaosVar,
    kParam10_PanChaosScale,
    kParam11_PanChaosVar,
    kParamNum // dont touch this
};


// enum's for each index
enum ChaosVariable {
    kChaosX,
    kChaosY,
    kChaosZ,
    kChaosNone, // bypass
    kChaosNum
};

enum FilterType {
    kParam3TypeLP,
    kParam3TypeHP,
    kParam3TypeBP,
    kParam3TypeBR,
    kParam3TypeNum
};



enum DistortionType {
    kParam7Type1,   // waveshaper 1
    kParam7Type2,   // waveshaper 3
    kParam7Type3,   // foldback distorion
    kParam7TypeNum
};


const char* const kParamName[] = {
	"Global Chaos Rate",
	"Global Chaos Scale",
	"Filter Type",
	"Filter Q",
	"Filter Chaos Scale",
	"Filter Chaos Variable",
	"Distortion Type",
	"Distortion Chaos Scale",
	"Distortion Chaos Variable",
	"Panning Chaos Scale",
	"Panning Chaos Variable"
};

// chaos index names
#define CHAOS_X "X"
#define CHAOS_Y "Y"
#define CHAOS_Z "Z"
#define CHAOS_NONE "<none>"




// min, max, default values, and names

#define CHAOS_SCALE_MIN 0.0
#define CHAOS_SCALE_MAX 1.0

#define                     ParamName1         "Global Chaos Rate"
const float                 kParam1Default      = 0.01;
const float                 kParam1Min          = 0.0001;
const float                 kParam1Max          = 1.0;

#define                     ParamName2         "Global Chaos Scale"
const float                 kParam2Default      = 0.5;
const float                 kParam2Min          = 0.0;
const float                 kParam2Max          = 1.0;

// Indexed type
#define                     ParamName3         "Filter Type"
const int                   kParam3Default      = kParam3TypeHP;
const int                   kParam3Min          = kParam3TypeLP;
const int                   kParam3Max          = kParam3TypeNum - 1;
#define                     Param3TypeNameLP   "LP"
#define                     Param3TypeNameHP   "HP"
#define                     Param3TypeNameBP   "BP"
#define                     Param3TypeNameBR   "BR"

#define                     ParamName4          "Filter Q"
const float                 kParam4Default      = PARAM_DEFAULTS[paramID_Q];
const float                 kParam4Min          = LIMITS_MIN[paramID_Q];
const float                 kParam4Max          = LIMITS_MAX[paramID_Q];

#define                     ParamName5          "Filter Chaos Scale"
const float                 kParam5Default      = 0.5;
const float                 kParam5Min          = CHAOS_SCALE_MIN;
const float                 kParam5Max          = CHAOS_SCALE_MAX;

// Indexed Type
#define                     ParamName6          "Filter Chaos Variable"
const int                   kParam6Default      = kChaosY;
const int                   kParam6Min          = kChaosX;
const int                   kParam6Max          = kChaosNum - 1;

// Indexed Type
#define                     ParamName7          "Distortion Type"
const int                   kParam7Default      = kParam7Type1;
const int                   kParam7Min          = kParam7Type1;
const int                   kParam7Max          = kParam7TypeNum - 1;
#define                     Param7TypeName1     "Shaper 1"
#define                     Param7TypeName2     "Shaper 3"
#define                     Param7TypeName3     "Foldback Distortion"

#define                     ParamName8          "Distortion Chaos Scale"
const float                 kParam8Default      = 0.5;
const float                 kParam8Min          = CHAOS_SCALE_MIN;
const float                 kParam8Max          = CHAOS_SCALE_MAX;

#define                     ParamName9          "Distortion Chaos Variable"
const int                   kParam9Default      = kChaosZ;
const int                   kParam9Min          = kChaosX;
const int                   kParam9Max          = kChaosNum - 1;

#define                     ParamName10         "Panning Chaos Scale"
const float                 kParam10Default      = 0.5;
const float                 kParam10Min          = CHAOS_SCALE_MIN;
const float                 kParam10Max          = CHAOS_SCALE_MAX;

#define                     ParamName11         "Panning Chaos Variable"
const int                   kParam11Default      = kChaosX;
const int                   kParam11Min          = kChaosX;
const int                   kParam11Max          = kChaosNum - 1;





#pragma mark - AU

#ifdef AUDIO_UNIT_PLUG

/************************************************************************************************
 Audio Unit Specific Parameters
 
 Last Updated by: GW
 Last Updated on: 2012/11/12
 ************************************************************************************************/

// Parameter names
static CFStringRef          kParamName1         = CFSTR(ParamName1);
static CFStringRef          kParamName2         = CFSTR(ParamName2);
static CFStringRef          kParamName3         = CFSTR(ParamName3);
static CFStringRef          kParamName4         = CFSTR(ParamName4);
static CFStringRef          kParamName5         = CFSTR(ParamName5);
static CFStringRef          kParamName6         = CFSTR(ParamName6);
static CFStringRef          kParamName7         = CFSTR(ParamName7);
static CFStringRef          kParamName8         = CFSTR(ParamName8);
static CFStringRef          kParamName9         = CFSTR(ParamName9);
static CFStringRef          kParamName10        = CFSTR(ParamName10);
static CFStringRef          kParamName11        = CFSTR(ParamName11);


// Param 3 Index Names
static CFStringRef          kParam3TypeNameHP   = CFSTR(Param3TypeNameHP);
static CFStringRef          kParam3TypeNameLP   = CFSTR(Param3TypeNameLP);
static CFStringRef          kParam3TypeNameBP   = CFSTR(Param3TypeNameBP);
static CFStringRef          kParam3TypeNameBR   = CFSTR(Param3TypeNameBR);


// Param 7 Index Names
static CFStringRef          kParam7TypeName1    = CFSTR(Param7TypeName1);
static CFStringRef          kParam7TypeName2    = CFSTR(Param7TypeName2);
static CFStringRef          kParam7TypeName3    = CFSTR(Param7TypeName3);


// Chaos index names
static CFStringRef          kChaosNameX         = CFSTR(CHAOS_X);
static CFStringRef          kChaosNameY         = CFSTR(CHAOS_Y);
static CFStringRef          kChaosNameZ         = CFSTR(CHAOS_Z);
static CFStringRef          kChaosNameNone      = CFSTR(CHAOS_NONE);



#pragma mark - VST

#elif VST_PLUG

/************************************************************************************************
 VST Specific Parameters
 
 Last Updated by: CF
 Last Updated on: 2012/11/12
 ************************************************************************************************/

// parameter mapping functions
#define float2GlobalChaosRate(x)		( ((x) * (kParam1Max - kParam1Min)) + kParam1Min )
#define float2GlobalChaosScale(x)		( (x) ) // no mapping required
#define float2FilterType(x)				( (int)((x) * kParam3Max) )
#define float2FilterQ(x)				( ((x) * (kParam4Max - kParam4Min)) + kParam4Min )
#define float2FilterChaosScale(x)		( ((x) * (kParam5Max - kParam5Min)) + kParam5Min )
#define float2FilterChaosVar(x)			( (int)((x) * kParam6Max) )
#define float2DistortionType(x)			( (int)((x) * kParam7Max) )
#define float2DistortionChaosScale(x)	( ((x) * (kParam8Max - kParam8Min)) + kParam8Min )
#define float2DistortionChaosVar(x)		( (int)((x) * kParam9Max) )
#define float2PanningChaosScale(x)		( ((x) * (kParam10Max - kParam10Min)) + kParam10Min )
#define float2PanningChaosVar(x)		( (int)((x) * kParam11Max) )

#define GlobalChaosRate2float(x)		( ((x) - kParam1Min) / (kParam1Max - kParam1Min) )
#define GlobalChaosScale2float(x)		( (x) ) // no mapping required
#define FilterType2float(x)				( ((float)(x)) / ((float)kParam3Max) )
#define FilterQ2float(x)				( ((x) - kParam4Min) / (kParam4Max - kParam4Min) )
#define FilterChaosScale2float(x)		( ((x) - kParam5Min) / (kParam5Max - kParam5Min) )
#define FilterChaosVar2float(x)			( ((float)(x)) / ((float)kParam6Max) )
#define DistortionType2float(x)			( ((float)(x)) / ((float)kParam7Max) )
#define DistortionChaosScale2float(x)	( ((x) - kParam8Min) / (kParam8Max - kParam8Min) )
#define DistortionChaosVar2float(x)		( ((float)(x)) / ((float)kParam9Max) )
#define PanningChaosScale2float(x)		( ((x) - kParam10Min) / (kParam10Max - kParam10Min) )
#define PanningChaosVar2float(x)		( ((float)(x)) / ((float)kParam11Max) )

// height & width of custom GUI editor window
const short EDITOR_WINDOW_HEIGHT	= 200;
const short EDITOR_WINDOW_WIDTH		= 400;


#pragma mark - RE

#elif RE_PLUG

/************************************************************************************************
 RE Specific Parameters
 
 Last Updated by: GW
 Last Updated on: 2012/11/11
 ************************************************************************************************/







#endif // plug ifdef






#endif // Chaos_Parameters_h ifndef



